package reefrock.interaction.leapmotion;

import java.util.Iterator;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Frame;
import com.leapmotion.leap.Gesture;
import com.leapmotion.leap.GestureList;
import com.leapmotion.leap.Hand;
import com.leapmotion.leap.HandList;
import com.leapmotion.leap.InteractionBox;
import com.leapmotion.leap.Vector;

import processing.core.PApplet;

public class LeapMotionInteraction
{
	private PApplet parent;
	private Controller leapMotionController;
	
	private Frame currentFrame;
	private Frame previousFrame;
	
	private GestureList gestures;
	
	private HandList hands;
	private Hand leftHand;
	private Hand rightHand;
	
	private float minSize;
	private float maxSize;
	private float rightEllipseSize;
	private float leftEllipseSize;
	
	private InteractionBox interactionBox;
	private Vector rightHandLocation;
	private Vector leftHandLocation;
	private Vector previousRightHandLocation;
	private Vector previousLeftHandLocation;
	private boolean isRightPressed = false;
	private boolean isRightPressedDirty = false;
	private boolean isLeftPressed = false;
	private boolean isLeftPressedDirty = false;
	
	public LeapMotionInteraction(PApplet parent)
	{
		this.parent = parent;
		
		leapMotionController = new Controller();
		leapMotionController.enableGesture(Gesture.Type.TYPE_SCREEN_TAP);
		
		currentFrame = leapMotionController.frame();
	}
	
	public void setMinMaxSizes(int dimensionX, int dimensionY)
	{
		float cellWidth = parent.displayWidth / dimensionX;
		float cellHeight = parent.displayHeight / dimensionY;
		
		if(cellHeight <= cellWidth)
		{
			minSize = cellHeight * 0.25f;
			maxSize = cellHeight * 4f;
		}
		else
		{
			minSize = cellWidth * 0.25f;
			maxSize = cellWidth * 4f;
		}
	}
	
	public void draw()
	{
		getFrames();
		getInteractionBox();
		getGestures();
		getHands();
		drawHands();
	}
	
	public void drawHands()
	{
		if(leftHand != null && interactionBox.isValid())
		{
			detectInteractions(leftHand, true);
			drawHand(leftHand, true, parent.color(128,128,0,200), interactionBox);
		}
		else
		{
			leftHandLocation = null;
			previousLeftHandLocation = null;
			isLeftPressed = false;
			isLeftPressedDirty = false;
		}
		
		if(rightHand != null && interactionBox.isValid())
		{
			detectInteractions(rightHand, false);
			drawHand(rightHand, false, parent.color(128,0,128,200), interactionBox);
		}
		else
		{
			rightHandLocation = null;
			previousRightHandLocation = null;
			isRightPressed = false;
			isRightPressedDirty = false;
		}
	}
	
	public void detectInteractions(Hand hand, boolean isLeft)
	{
		Vector handPosition = hand.sphereCenter();
		Vector normalizedHandPosition = interactionBox.normalizePoint(handPosition);
		
		if(isLeft)
		{
			previousLeftHandLocation = leftHandLocation;
			leftHandLocation = normalizedHandPosition;
			
			leftEllipseSize = scaleRange(normalizedHandPosition.getZ(), 0f, 1f, minSize, maxSize);
			
			if(leftEllipseSize <= maxSize / 3f)
			{
				if(isLeftPressed == false)
				{
					isLeftPressedDirty = true;
				}
				isLeftPressed = true;
			}
			else
			{
				if(isLeftPressed == true)
				{
					isLeftPressedDirty = true;
				}
				isLeftPressed = false;
			}
		}
		else
		{
			previousRightHandLocation = rightHandLocation;
			rightHandLocation = normalizedHandPosition;
			
			rightEllipseSize = scaleRange(normalizedHandPosition.getZ(), 0f, 1f, minSize, maxSize);
			
			if(rightEllipseSize <= maxSize / 3f)
			{
				if(isRightPressed == false)
				{
					isRightPressedDirty = true;
				}
				isRightPressed = true;
			}
			else
			{
				if(isRightPressed == true)
				{
					isRightPressedDirty = true;
				}
				isRightPressed = false;
			}
		}
	}
	
	public void drawHand(Hand hand, boolean isLeft, int handColor, InteractionBox interactionBox)
	{
		parent.pushMatrix();
		
		int oldFill = parent.g.fillColor;
		parent.fill(handColor);
		
		if(isLeft)
		{
			Vector normalizedHandPosition = leftHandLocation;
			parent.ellipse(normalizedHandPosition.getX() * parent.displayWidth, parent.displayHeight - normalizedHandPosition.getY() * parent.displayHeight, leftEllipseSize, leftEllipseSize);
		}
		else
		{
			Vector normalizedHandPosition = rightHandLocation;
			parent.ellipse(normalizedHandPosition.getX() * parent.displayWidth, parent.displayHeight - normalizedHandPosition.getY() * parent.displayHeight, rightEllipseSize, rightEllipseSize);
		}
		
		parent.fill(oldFill);
		parent.popMatrix();
	}
	
	public float scaleRange(float oldValue, float oldMin, float oldMax, float newMin, float newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	public void getFrames()
	{
		previousFrame = currentFrame;
		currentFrame = leapMotionController.frame();
	}
	
	public void getInteractionBox()
	{
		interactionBox = currentFrame.interactionBox();
	}
	
	public void getGestures()
	{
		gestures = currentFrame.gestures();
		if(gestures.count() > 0)
		{
			System.out.println("Got gestures!");
		}
	}
	
	public void getHands()
	{
		hands = currentFrame.hands();
		if(hands.count() > 0)
		{
			System.out.println("Got hands!");
			Iterator<Hand> handIterator = hands.iterator();
			System.out.print("Hands ID:\t");
			int validHandCount = 0;
			while(handIterator.hasNext())
			{
				Hand currentHandObject = handIterator.next();
				if(currentHandObject.isValid())
				{
					System.out.print(currentHandObject.id() + "\t");
					validHandCount++;
				}
			}
			System.out.println();
			System.out.println("Got " + validHandCount + " valid hands.");
		
			if(leftHand != null && currentFrame.hand(leftHand.id()) != null && currentFrame.hand(leftHand.id()).isValid())
			{
				leftHand = currentFrame.hand(leftHand.id());
			}
			else
			{
				leftHand = null;
			}
			
			if(rightHand != null && currentFrame.hand(rightHand.id()) != null && currentFrame.hand(rightHand.id()).isValid())
			{
				rightHand = currentFrame.hand(rightHand.id());
			}
			else
			{
				rightHand = null;
			}
			
			if(leftHand == null && rightHand != null && hands.count() > 1)
			{
				Iterator<Hand> handsIterator = hands.iterator();
				while(handsIterator.hasNext())
				{
					Hand currentHand = handsIterator.next();
					if(currentHand.id() != rightHand.id() && currentHand.isValid())
					{
						leftHand = currentHand;
						break;
					}
				}
			}
			else if(leftHand != null && rightHand == null && hands.count() > 1)
			{
				Iterator<Hand> handsIterator = hands.iterator();
				while(handsIterator.hasNext())
				{
					Hand currentHand = handsIterator.next();
					if(currentHand.id() != leftHand.id() && currentHand.isValid())
					{
						rightHand = currentHand;
						break;
					}
				}
			}
			
			if(leftHand == null && rightHand == null && hands.count() > 0)
			{
				Iterator<Hand> handsIterator = hands.iterator();
				while(handsIterator.hasNext())
				{
					Hand currentHand = handsIterator.next();
					if(currentHand.isValid() && currentHand.isLeft())
					{
						leftHand = currentHand;
					}
					else if(currentHand.isValid() && currentHand.isRight())
					{
						rightHand = currentHand;
					}
					
					if(leftHand != null && rightHand != null)
					{
						break;
					}
				}
			}
			
			if(leftHand != null)
			{
				System.out.println("Left Hand: X: " + leftHand.sphereCenter().getX() + ", Y: " + leftHand.sphereCenter().getY() + ", Z: " + leftHand.sphereCenter().getZ());
			}
			
			if(rightHand != null)
			{
				System.out.println("Right Hand: X: " + rightHand.sphereCenter().getX() + ", Y: " + rightHand.sphereCenter().getY() + ", Z: " + rightHand.sphereCenter().getZ());
			}
		}
	}

	public Vector getRightHandLocation()
	{
		return rightHandLocation;
	}

	public void setRightHandLocation(Vector rightHandLocation)
	{
		this.rightHandLocation = rightHandLocation;
	}

	public Vector getLeftHandLocation()
	{
		return leftHandLocation;
	}

	public void setLeftHandLocation(Vector leftHandLocation)
	{
		this.leftHandLocation = leftHandLocation;
	}

	public Vector getPreviousRightHandLocation()
	{
		return previousRightHandLocation;
	}

	public void setPreviousRightHandLocation(Vector previousRightHandLocation)
	{
		this.previousRightHandLocation = previousRightHandLocation;
	}

	public Vector getPreviousLeftHandLocation()
	{
		return previousLeftHandLocation;
	}

	public void setPreviousLeftHandLocation(Vector previousLeftHandLocation)
	{
		this.previousLeftHandLocation = previousLeftHandLocation;
	}

	public boolean isRightPressed()
	{
		return isRightPressed;
	}

	public void setRightPressed(boolean isRightPressed)
	{
		this.isRightPressed = isRightPressed;
	}

	public boolean isLeftPressed()
	{
		return isLeftPressed;
	}

	public void setLeftPressed(boolean isLeftPressed)
	{
		this.isLeftPressed = isLeftPressed;
	}

	public boolean isRightPressedDirty()
	{
		return isRightPressedDirty;
	}

	public void setRightPressedDirty(boolean isRightPressedDirty)
	{
		this.isRightPressedDirty = isRightPressedDirty;
	}

	public boolean isLeftPressedDirty()
	{
		return isLeftPressedDirty;
	}

	public void setLeftPressedDirty(boolean isLeftPressedDirty)
	{
		this.isLeftPressedDirty = isLeftPressedDirty;
	}
}
