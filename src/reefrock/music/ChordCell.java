package reefrock.music;

import reefrock.shared.Point;

public class ChordCell
{
	private String chord;
	private Point origin;
	private Point center;
	private float width;
	private float height;
	private boolean isOn;
	
	public ChordCell(String chord, Point origin, float width, float height)
	{
		this(chord, origin, width, height, new Point(origin.x + width / 2, origin.y + height / 2));
	}
	
	public ChordCell(String chord, float width, float height, Point center)
	{
		this(chord, new Point(center.x - width / 2, center.y - height / 2), width, height, center);
	}
	
	public ChordCell(String chord, Point origin, Point center)
	{
		this(chord, origin, center.x - origin.x, center.y - origin.y, center);
	}
	
	public ChordCell(String chord, Point origin, float width, float height, Point center)
	{
		this.chord = chord;
		this.origin = new Point(origin);
		this.center = new Point(center);
		this.width = width;
		this.height = height;
		this.isOn = false;
	}

	public String getChord()
	{
		return chord;
	}

	public void setChord(String chord)
	{
		this.chord = chord;
	}

	public Point getOrigin()
	{
		return origin;
	}

	public void setOrigin(Point origin)
	{
		this.origin = origin;
	}

	public Point getCenter()
	{
		return center;
	}

	public void setCenter(Point center)
	{
		this.center = center;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}
	
	public boolean getIsOn()
	{
		return this.isOn;
	}
	
	public void setIsOn(boolean isOn)
	{
		this.isOn = isOn;
	}
	
	public void toggleIsOn()
	{
		this.isOn = !this.isOn;
	}
	
	@Override
	public String toString()
	{
		return "[CHORD: " + this.chord + ", ORIGIN: (" + this.origin.x + ", " + 
	this.origin.y + "), WIDTH: " + this.width + ", HEIGHT: " + this.height + 
	", CENTER: (" + this.center.x + ", " + this.center.y + ")" + ", IS ON: " + 
	this.isOn + "]";
	}
	
	public boolean pointInCell(Point p)
	{
		if(origin.x <= p.x && p.x <= origin.x + width && origin.y <= p.y && p.y <= origin.y + height)
		{
			return true;
		}
		return false;
	}
	
	public boolean equalTo(ChordCell cell)
	{
		if(this.center.equalsEpsilon2D(cell.getCenter()))
		{
			return true;
		}
		return false;
	}
}
