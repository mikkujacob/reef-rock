package reefrock.organisms.alife;

import java.util.Random;

import processing.core.PImage;
import reefrock.music.ChordCell;
import reefrock.music.ChordMatrix;
import reefrock.shared.Point;

public class Ant
{
	private Point position;
	private Orientation orientation;
	private int sizeX, sizeY;
	private static Random random = new Random();
	private ChordMatrix matrix;
	private StepSize stepSize;
	private PImage antImage;
	
	public enum Orientation
	{
		RIGHT, LEFT, UP, DOWN
	};

	public enum StepSize
	{
		ONE, TWO, THREE, FOUR
	};
	
	public Ant(int x, int y, Orientation orientation, int sizeX, int sizeY, ChordMatrix matrix, PImage antImage)
	{
		this.position = new Point(x, y);
		this.orientation = orientation;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.matrix = matrix;
//		this.stepSize = StepSize.values()[random.nextInt(StepSize.values().length)];
		this.stepSize = StepSize.ONE;
//		this.antImage = this.matrix.getParent().loadImage("data/ant.png");
		this.antImage = antImage;
	}
	
	public Ant(int sizeX, int sizeY, ChordMatrix matrix, PImage antImage)
	{
		this(Ant.random.nextInt(sizeX), Ant.random.nextInt(sizeY), 
				Orientation.values()[Ant.random.nextInt(Orientation.values().length)], 
				sizeX, sizeY, matrix, antImage);
	}
	
	public Ant(int x, int y, int sizeX, int sizeY, ChordMatrix matrix, PImage antImage)
	{
		this(x, y, Orientation.values()[Ant.random.nextInt(Orientation.values().length)], 
				sizeX, sizeY, matrix, antImage);
	}
	
	public void nextMove()
	{
		ChordCell cell = matrix.getCellFromIndexPoint(position);
		
		if(cell.getIsOn())
		{
			turnLeft();
		}
		else
		{
			turnRight();
		}
		
		cell.toggleIsOn();
		
		if(stepSize.compareTo(StepSize.ONE) == 0)
		{
			marchOne();
		}
		else if(stepSize.compareTo(StepSize.TWO) == 0)
		{
			marchTwo();
		}
		else if(stepSize.compareTo(StepSize.THREE) == 0)
		{
			marchThree();
		}
		else if(stepSize.compareTo(StepSize.FOUR) == 0)
		{
			marchFour();
		}
	}
	
	public boolean turnRight()
	{
		if(orientation.compareTo(Orientation.LEFT) == 0)
		{
			orientation = Orientation.UP;
			return true;
		}
		else if(orientation.compareTo(Orientation.DOWN) == 0)
		{
			orientation = Orientation.LEFT;
			return true;
		}
		else if(orientation.compareTo(Orientation.RIGHT) == 0)
		{
			orientation = Orientation.DOWN;
			return true;
		}
		else if(orientation.compareTo(Orientation.UP) == 0)
		{
			orientation = Orientation.RIGHT;
			return true;
		}
		
		return false;
	}
	
	public boolean turnLeft()
	{
		if(orientation.compareTo(Orientation.LEFT) == 0)
		{
			orientation = Orientation.DOWN;
			return true;
		}
		else if(orientation.compareTo(Orientation.DOWN) == 0)
		{
			orientation = Orientation.RIGHT;
			return true;
		}
		else if(orientation.compareTo(Orientation.RIGHT) == 0)
		{
			orientation = Orientation.UP;
			return true;
		}
		else if(orientation.compareTo(Orientation.UP) == 0)
		{
			orientation = Orientation.LEFT;
			return true;
		}
		
		return false;
	}
	
	public void marchOne()
	{
		if(orientation.compareTo(Orientation.LEFT) == 0)
		{
			position.x = (sizeX + position.x - 1) % sizeX;
		}
		else if(orientation.compareTo(Orientation.DOWN) == 0)
		{
			position.y = (position.y + 1) % sizeY;
		}
		else if(orientation.compareTo(Orientation.RIGHT) == 0)
		{
			position.x = (position.x + 1) % sizeX;
		}
		else if(orientation.compareTo(Orientation.UP) == 0)
		{
			position.y = (sizeY + position.y - 1) % sizeY;
		}
	}
	
	public void marchTwo()
	{
		if(orientation.compareTo(Orientation.LEFT) == 0)
		{
			position.x = (sizeX + position.x - 2) % sizeX;
		}
		else if(orientation.compareTo(Orientation.DOWN) == 0)
		{
			position.y = (position.y + 2) % sizeY;
		}
		else if(orientation.compareTo(Orientation.RIGHT) == 0)
		{
			position.x = (position.x + 2) % sizeX;
		}
		else if(orientation.compareTo(Orientation.UP) == 0)
		{
			position.y = (sizeY + position.y - 2) % sizeY;
		}
	}
	
	public void marchThree()
	{
		if(orientation.compareTo(Orientation.LEFT) == 0)
		{
			position.x = (sizeX + position.x - 3) % sizeX;
		}
		else if(orientation.compareTo(Orientation.DOWN) == 0)
		{
			position.y = (position.y + 3) % sizeY;
		}
		else if(orientation.compareTo(Orientation.RIGHT) == 0)
		{
			position.x = (position.x + 3) % sizeX;
		}
		else if(orientation.compareTo(Orientation.UP) == 0)
		{
			position.y = (sizeY + position.y - 3) % sizeY;
		}
	}
	
	public void marchFour()
	{
		if(orientation.compareTo(Orientation.LEFT) == 0)
		{
			position.x = (sizeX + position.x - 4) % sizeX;
		}
		else if(orientation.compareTo(Orientation.DOWN) == 0)
		{
			position.y = (position.y + 4) % sizeY;
		}
		else if(orientation.compareTo(Orientation.RIGHT) == 0)
		{
			position.x = (position.x + 4) % sizeX;
		}
		else if(orientation.compareTo(Orientation.UP) == 0)
		{
			position.y = (sizeY + position.y - 4) % sizeY;
		}
	}

	public Point getPosition()
	{
		return position;
	}

	public void setPosition(Point position)
	{
		this.position = position;
	}

	public Orientation getOrientation()
	{
		return orientation;
	}

	public void setOrientation(Orientation orientation)
	{
		this.orientation = orientation;
	}
	
	public boolean isNeighborOf(Ant a)
	{
		Point p = a.getPosition();
		if((p.x == position.x - 1 && p.y == position.y - 1) ||
				(p.x == position.x && p.y == position.y - 1) ||
				(p.x == position.x + 1 && p.y == position.y - 1) ||
				(p.x == position.x - 1 && p.y == position.y) ||
				(p.x == position.x + 1 && p.y == position.y) ||
				(p.x == position.x - 1 && p.y == position.y + 1) ||
				(p.x == position.x && p.y == position.y + 1) ||
				(p.x == position.x - 1 && p.y == position.y + 1))
		{
			return true;
		}
		return false;
	}
	
	public boolean isSamePositionAs(Ant a)
	{
		Point p = a.getPosition();
		if(p.x == position.x && p.y == position.y)
		{
			return true;
		}
		return false;
	}

	public ChordMatrix getMatrix()
	{
		return matrix;
	}

	public void setMatrix(ChordMatrix matrix)
	{
		this.matrix = matrix;
	}
	
	public float getOrientationAngle()
	{
		float angle = 0;
		if(orientation.compareTo(Orientation.RIGHT) == 0)
		{
			angle = 0;
		}
		else if(orientation.compareTo(Orientation.DOWN) == 0)
		{
			angle = (float) (Math.PI / 2f);
		}
		else if(orientation.compareTo(Orientation.LEFT) == 0)
		{
			angle = (float) (Math.PI);
		}
		else if(orientation.compareTo(Orientation.UP) == 0)
		{
			angle = (float) (3 * Math.PI / 2f);
		}
		return angle;
	}

	public PImage getAntImage()
	{
		return antImage;
	}

	public void setAntImage(PImage antImage)
	{
		this.antImage = antImage;
	}
}
