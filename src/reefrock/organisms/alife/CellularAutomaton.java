package reefrock.organisms.alife;

import java.util.Random;

import processing.core.PImage;
import reefrock.music.ChordCell;
import reefrock.music.ChordMatrix;
import reefrock.shared.Point;

public class CellularAutomaton
{
	private Point position;
	private int sizeX, sizeY;
	private static Random random = new Random();
	private ChordMatrix matrix;
	private PImage cellularAutomatonImage;
	
	public CellularAutomaton(int x, int y, int sizeX, int sizeY, ChordMatrix matrix, PImage automatonImage)
	{
		this.position = new Point(x, y);
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.matrix = matrix;
		this.setCellularAutomatonImage(automatonImage);
	}
	
	public CellularAutomaton(int sizeX, int sizeY, ChordMatrix matrix, PImage automatonImage)
	{
		this(CellularAutomaton.random.nextInt(sizeX), CellularAutomaton.random.nextInt(sizeY), 
				sizeX, sizeY, matrix, automatonImage);
	}
	
	public void nextMove()
	{
		int count = countOnNeighbours();
		ChordCell cell = matrix.getCellFromIndexPoint(position);
		if(count == 3)
		{
			//Cell state is alive in next move.
			cell.setIsOn(true);
		}
		else if(count == 4)
		{
			//Cell state is same in next move.
		}
		else
		{
			//Cell state is dead in next move.
			cell.setIsOn(false);
		}
	}
	
	public int countOnNeighbours()
	{
		int onCellCount = 0;
		for(int i = 0, x = ((int) (sizeX + position.x - 1)) % sizeX; i < 3; i++, x = (x + 1) % sizeX)
		{
			for(int j = 0, y = ((int) (sizeY + position.y - 1)) % sizeY; j < 3; j++, y = (y + 1) % sizeY)
			{
				ChordCell cell = matrix.getMatrix()[x][y];
				if(cell.getIsOn())
				{
					onCellCount++;
				}
			}
		}
		return onCellCount;
	}
	
	public Point getPosition()
	{
		return position;
	}

	public void setPosition(Point position)
	{
		this.position = position;
	}
	
	public boolean isNeighborOf(CellularAutomaton c)
	{
		Point p = c.getPosition();
		if((p.x == position.x - 1 && p.y == position.y - 1) ||
				(p.x == position.x && p.y == position.y - 1) ||
				(p.x == position.x + 1 && p.y == position.y - 1) ||
				(p.x == position.x - 1 && p.y == position.y) ||
				(p.x == position.x + 1 && p.y == position.y) ||
				(p.x == position.x - 1 && p.y == position.y + 1) ||
				(p.x == position.x && p.y == position.y + 1) ||
				(p.x == position.x - 1 && p.y == position.y + 1))
		{
			return true;
		}
		return false;
	}
	
	public boolean isSamePositionAs(CellularAutomaton c)
	{
		Point p = c.getPosition();
		if(p.x == position.x && p.y == position.y)
		{
			return true;
		}
		return false;
	}

	public ChordMatrix getMatrix()
	{
		return matrix;
	}

	public void setMatrix(ChordMatrix matrix)
	{
		this.matrix = matrix;
	}

	public PImage getCellularAutomatonImage()
	{
		return cellularAutomatonImage;
	}

	public void setCellularAutomatonImage(PImage cellularAutomatonImage)
	{
		this.cellularAutomatonImage = cellularAutomatonImage;
	}
}
