package reefrock.shared;

import processing.core.PVector;

public class Point
{
	public float x, y, z, epsilon = 0.1f;
	
	public Point()
	{
		x = y = z = 0f;
	}
	
	public Point(float x, float y)
	{
		this.x = x;
		this.y = y;
		this.z = 0;
	}
	
	public Point(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Point(Point p)
	{
		this.x = p.x;
		this.y = p.y;
		this.z = p.z;
	}
	
	public Point(PVector p)
	{
		this.x = p.x;
		this.y = p.y;
		this.z = p.z;
	}
	
	public Boolean equals(Point q)
	{
		return equals(q.x, q.y, q.z);
	}
	
	public Boolean equals(float qx, float qy, float qz)
	{
		if(this.x == qx && this.y == qy && this.z == qz)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Boolean equalsEpsilon2D(Point q)
	{
		return equalsEpsilon2D(q.x, q.y);
	}
	
	public Boolean equalsEpsilon2D(float qx, float qy)
	{
		if(distance2D(this.x, this.y, qx, qy) < epsilon)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Boolean equalsEpsilon3D(Point q)
	{
		return equalsEpsilon3D(q.x, q.y, q.z);
	}
	
	public Boolean equalsEpsilon3D(float qx, float qy, float qz)
	{
		if(distance3D(this.x, this.y, this.z, qx, qy, qz) < epsilon)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public float distance2D(float x1, float y1, float x2, float y2)
	{
		return (float) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
	
	public float distance3D(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		return (float) Math.sqrt((z2 - z1) * (z2 - z1) +(y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
	
	public float distance2(float x1, float y1, float x2, float y2)
	{
		return ((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
}
