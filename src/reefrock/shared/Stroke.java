package reefrock.shared;

import java.util.ArrayList;

public class Stroke
{
	private ArrayList<Point> stroke;
	
	public Stroke()
	{
		stroke = new ArrayList<Point>();
	}
	
	public Stroke(Stroke s)
	{
		stroke = new ArrayList<Point>();
		for(Point p : s.getStroke())
		{
			stroke.add(new Point(p));
		}
	}
	
	public ArrayList<Point> getStroke()
	{
		return stroke;
	}
	
	public void addPoint(Point p)
	{
		stroke.add(new Point(p));
	}
}
