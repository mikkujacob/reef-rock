README
______

This is the Java repository for the ReefRock interactive music-making application. 

CONCEPT:

Create relaxing music by interacting with an ecosystem of A-Life organisms (Conway's Game of Life, Langton's Ants, and Flocking Boids). In this version, the cellular automata are coral, ants are seahorses, and boids are fish. This project is also a (somewhat heavy-handed) attempt at informal learning about conservation, balance, harmony, and human intervention in ecosystems. 

The cellular automata and ants interact with each other using chemical signaling (turning the cells of a tone matrix on and off), while the boids control how the tone matrix is played. The centroid of the boids is the central playhead (multiple playheads for local clusters of boids can be toggled as well). As a human, you can directly toggle the tone matrix, as well as add/remove organisms from the ecosystem. The tone matrix can toggle different scales, root notes, chords vs. notes, and various other customizations to achieve a desired sound.

RUNNING THE CODE:

1. Run the following class file in the project bin directory (after successful compilation with all the dependencies in the lib directory): reefrock.application.ReefRockApplication.class

VIEWING AND RUNNING THE CODE FROM ECLIPSE:

1. Download Eclipse from https://eclipse.org/downloads/ .
2. Git clone codebase or download and unzip the codebase.
3. Import project using File->Import->General->Existing Projects Into Workspace, then click Next.
4. Select the codebase root directory and click Finish.
5. Source is inside src, libraries are inside lib, documentation is inside docs, and data is inside data.
6. Open reefrock.application.ReefRockApplication.java in src.
7. Click Run As Application.
